﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Terrapass.Extensions.Unity;

public class ParallaxedObject : MonoBehaviour
{
	[SerializeField]
	private AbstractParallaxInputSource parallaxInputSource;

	[SerializeField]
	private float parallaxAmount;

	private Vector3 initialPosition;

	void Start()
	{
		this.EnsureRequiredFieldsAreSetInEditor();

		this.initialPosition = this.transform.position;
	}

	void FixedUpdate()
	{
		// TODO: Try other methods
		var parallaxOffset = parallaxAmount * parallaxInputSource.ParallaxVector;
		
		transform.position = initialPosition + Vector3.right*parallaxOffset.x + Vector3.up*parallaxOffset.y;
	}
}
