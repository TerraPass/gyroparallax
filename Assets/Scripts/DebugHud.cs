using UnityEngine;
using UnityEngine.UI;

using Terrapass.Extensions.Unity;

public class DebugHud : MonoBehaviour
{
    private const string PARALLAX_VECTOR_FORMAT     = "({0},{1})";
    private const string PARALLAX_VECTOR_ERROR      = "(?,?)";
    private const string NOT_AVAILABLE_ERROR_FORMAT = "{0} is not available; parallax input might not work properly";

    [SerializeField]
    private Text parallaxVectorText;

    [SerializeField]
    private Text errorText;

    [SerializeField]
    private SelectableParallaxInputSource selectableParallaxInputSource;

    void Start()
    {
        this.EnsureRequiredFieldsAreSetInEditor();
    }

    void Update()
    {
        if (selectableParallaxInputSource.IsAvailable)
        {
            parallaxVectorText.text = string.Format(
                PARALLAX_VECTOR_FORMAT,
                selectableParallaxInputSource.ParallaxVector.x,
                selectableParallaxInputSource.ParallaxVector.y
            );

            errorText.enabled = false;
        }
        else
        {
            parallaxVectorText.text = PARALLAX_VECTOR_ERROR;

            errorText.enabled = true;
            errorText.text = string.Format(NOT_AVAILABLE_ERROR_FORMAT, selectableParallaxInputSource.SelectedInputSourceKind);
        }
    }
}
