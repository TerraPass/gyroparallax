﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AbstractParallaxInputSource : MonoBehaviour
{
	public abstract bool IsAvailable {get;}
	public abstract Vector2 ParallaxVector {get;}
}
