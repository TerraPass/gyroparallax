﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroscopeParallaxInputSource : AbstractParallaxInputSource
{
	private Vector2 parallaxVector;

	public override bool IsAvailable
	{
		get {
			return SystemInfo.supportsGyroscope;
		}
	}

    public override Vector2 ParallaxVector
    {
        get {
            return parallaxVector;
        }
    }

	void Start()
	{
		parallaxVector = Vector2.zero;
	}

	void FixedUpdate()
	{
		parallaxVector = VectorUtils.ToVector2XY(Input.gyro.userAcceleration);
		// TODO: Try using attitude?
	}
}
