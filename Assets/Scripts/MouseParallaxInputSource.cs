﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseParallaxInputSource : AbstractParallaxInputSource
{
	private Vector2 parallaxVector;

	public override bool IsAvailable
	{
		get {
			return Input.mousePresent;
		}
	}

    public override Vector2 ParallaxVector
    {
        get {
            return parallaxVector;
        }
    }

	void Start()
	{
		parallaxVector = Vector2.zero;
	}

	void FixedUpdate()
	{
		parallaxVector = ScreenCenterDiff(Input.mousePosition);
	}

	private static Vector2 ScreenCenterDiff(Vector2 screenPosition)
	{
		// TODO: Cache?
		Vector2 screenCenter = new Vector2(Screen.width / 2, Screen.height / 2);
		return VectorUtils.ElementwiseMult(
			screenPosition - screenCenter,
			new Vector2(1.0f/screenCenter.x, 1.0f/screenCenter.y)
		);
	}
}
