using System;
using System.Collections.Generic;
using UnityEngine;

using Terrapass.Debug;
using Terrapass.Extensions.Unity;

public class SelectableParallaxInputSource : AbstractParallaxInputSource
{
    [Serializable]
    private struct InputSourceMapping
    {
        public InputSourceKind kind;
        public AbstractParallaxInputSource source;
    }

    [SerializeField]
    private InputSourceMapping[] inputSourceMappings;
    
    [SerializeField]
    private InputSourceKind selectedInputSourceKind;

    private IDictionary<InputSourceKind, AbstractParallaxInputSource> inputSources;

    public override bool IsAvailable
    {
        get {
            return inputSources.ContainsKey(selectedInputSourceKind) && inputSources[selectedInputSourceKind].IsAvailable;
        }
    }

    public override Vector2 ParallaxVector
    {
        get {
            try
            {
                return inputSources[selectedInputSourceKind].ParallaxVector;
            }
            catch (KeyNotFoundException e)
            {
                throw new InvalidOperationException(
                    string.Format(
                        "{1} does not support input source kind {2}: {3}",
                        this.GetType(),
                        selectedInputSourceKind.ToString(),
                        e.Message
                    )
                );
            }
        }
    }

    public InputSourceKind SelectedInputSourceKind
    {
        get {
            return selectedInputSourceKind;
        }
        set {
            selectedInputSourceKind = value;
        }
    }

    void Start()
    {
        this.EnsureRequiredFieldsAreSetInEditor();

        inputSources = new Dictionary<InputSourceKind, AbstractParallaxInputSource>();
        foreach (var mapping in inputSourceMappings)
        {
            DebugUtils.Assert(
                !inputSources.Keys.Contains(mapping.kind),
                string.Format(
                    "There must be only one mapping for every input source kind; {0} was mapped more than once",
                    mapping.kind
                )
            );

            inputSources.Add(mapping.kind, mapping.source);
        }
    }
}