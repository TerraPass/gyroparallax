﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccelerometerParallaxInputSource : AbstractParallaxInputSource
{
	//private static readonly Vector2 DEFAULT_SENSITIVITY = new Vector2(1.0f, 1.0f);
	private const float DEFAULT_LERP_AMOUNT = 0.5f;

	//[SerializeField]
	//private Vector2 parallaxSensitivity = DEFAULT_SENSITIVITY;

	[SerializeField]
	private float lerpAmount = DEFAULT_LERP_AMOUNT;

	private Vector2 parallaxVector;

    public override Vector2 ParallaxVector
    {
        get {
            return parallaxVector;
        }
    }

	public override bool IsAvailable
	{
		get {
			return SystemInfo.supportsAccelerometer;
		}
	}

	void Start()
	{
		parallaxVector = Vector2.zero;
	}

	void FixedUpdate()
	{
		parallaxVector = Vector2.Lerp(
			parallaxVector,
			VectorUtils.ToVector2XY(Input.acceleration),
			lerpAmount
		);

		//parallaxVector = VectorUtils.ToVector2XY(Input.acceleration);

		// TODO: If directly using acceleration as parallax doesn't work, try accumulating:
		// parallaxVector += VectorUtils.ElementwiseMult(
		// 	VectorUtils.ToVector2XY(Input.acceleration),
		// 	parallaxSensitivity
		// );
	}
}
