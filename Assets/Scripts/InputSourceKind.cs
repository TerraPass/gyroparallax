
public enum InputSourceKind
{
    None          = 0,
    Mouse         = 1,
    Accelerometer = 2,
    Gyroscope     = 3
}
