using UnityEngine;

using Terrapass.Extensions.Unity;

[RequireComponent(typeof(Collider))]
public class ClickInputSourceSelector : MonoBehaviour
{
    [SerializeField]
    private SelectableParallaxInputSource selectableInputSource;

    [SerializeField]
    private InputSourceKind selectedKind;

    void Start()
    {
        this.EnsureRequiredFieldsAreSetInEditor();
    }

    void OnMouseDown()
    {
        selectableInputSource.SelectedInputSourceKind = selectedKind;
    }
}
