using UnityEngine;

public class ConstantSpeedRotation : MonoBehaviour
{
    [SerializeField]
    private float angularSpeed = 45.0f;

    void FixedUpdate()
    {
        transform.Rotate(Vector3.up, angularSpeed * Time.fixedDeltaTime, Space.Self);
    }
}
