using UnityEngine;

public class VectorUtils
{
    public static Vector2 ToVector2XY(Vector3 vector)
    {
        return new Vector2(vector.x, vector.y);
    }

    public static Vector3 FromVector2XY(Vector2 vector)
    {
        return new Vector3(vector.x, vector.y, 0.0f);
    }

    public static Vector2 ElementwiseMult(Vector2 vector0, Vector2 vector1)
    {
        return new Vector2(vector0.x * vector1.x, vector0.y * vector1.y);
    }
}